
# Package

version       = "0.1.0"
author        = "monofuel"
description   = "nim llama.cpp interface"
license       = "MIT"
srcDir        = "src"
bin           = @["llama_cpp"]


# Dependencies

requires "nim >= 1.6.8"

before build:
  exec "cd llama.cpp && make LLAMA_CLBLAST=1 libllama.so"