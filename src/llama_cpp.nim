import
  std/[strformat, strutils, sequtils], ./llama_cpp_h

const numa = false
var init = false

const llama_model = "./llama.cpp/models/7B/ggml-model-q4_0.gguf"

const ctx_size = 512

# TODO separate embedding ctx
var ctx_params: llama_context_params

var cpp_model: ptr llama_model
var ctx: ptr llama_context

func `$`*(ctx: ptr llama_context, token: llama_token): string =
  let cstr = llama_token_to_str(ctx, token)
  if cstr != nil:
    result = $cstr
  else:
    result = ""


proc getLlama2CPPResponse*(systemPrompt: string, input: string): string =
  echo &"getLlama2CPPResponse()"

  # https://github.com/ggerganov/llama.cpp/blob/master/examples/simple/simple.cpp

  if not init:
    init = true
    llama_backend_init(numa)
    echo "backend init done"
    ctx_params = llama_context_default_params()
    ctx_params.n_ctx = ctx_size
    ctx_params.n_gpu_layers = 5

    cpp_model = llama_load_model_from_file(llama_model, ctx_params)
    echo "model loaded"
    ctx = llama_new_context_with_model(cpp_model, ctx_params)
    echo "context created"

  # var tokens: array[ctx_size, llama_token]
  var tokens = newSeq[llama_token](ctx_size)

  for i in 0 ..< tokens.len:
    tokens[i] = llama_token_bos(ctx)

  var add_bos = true

  let tok_input = systemPrompt & "\n" & input

  let num_input_tokens = llama_tokenize(ctx, tok_input.cstring, addr tokens[0],
      ctx_size.cint, add_bos)

  if num_input_tokens < 0:
    # Handle the error here - the return value tells you how many tokens would have been needed
    return ""

  let n_past = 0
  let n_threads = 8

  # TODO this don't work
  while llama_get_kv_cache_token_count(ctx) < ctx_size:
    let eval_result = llama_eval(ctx, addr tokens[0], tokens.len.cint,
        n_past.cint, n_threads.cint)
    if eval_result != 0:
      raise newException(Exception, &"llama_eval failed: {eval_result}")

    tokens.setLen(0) # Clearing the tokens list

    var candidates: seq[llama_token_data] = newSeq[llama_token_data](
        llama_n_vocab(ctx))
    let logitsPtr = llama_get_logits(ctx)
    echo "First logit: ", cast[ptr float32](cast[int](logitsPtr))[]

    # Fill candidates with token data
    for token_id in 0 ..< candidates.len:
      let valuePtr = cast[ptr float32](cast[int](logitsPtr) + token_id * sizeof(float32))
      let logit = valuePtr[]

      candidates[token_id] = llama_token_data(id: token_id, logit: logit, p: 0.0)

    let candidates_p = llama_token_data_array(data: candidates[0].addr,
        size: candidates.len.uint, sorted: false)

    let new_token_id = llama_sample_token_greedy(ctx, candidates_p.unsafeAddr)
    echo "New token ID: ", new_token_id

    if new_token_id == llama_token_eos(ctx):
      stderr.writeLine(" [end of text]")
      break
    else:
      let ch = llama_token_to_str(ctx, new_token_id)
      echo &"CH: {ch}"
      if ch != nil and ch != "":
        result = result & $ch
        tokens.add(new_token_id) # Push this new token for next evaluation





# TODO
# proc getLlamaCPPEmbeddings*(text: string): EmbeddingVector =



proc testLlama2*() =
  let prompt = "does pineapple belong on pizza?"
  let system = "You are a helpful but sassy AI. speak like Glados from Portal."
  echo &"$ {prompt}"
  let resp = getLlama2CPPResponse(system, prompt)
  echo "---"
  echo &">{resp}"


when isMainModule:
  testLlama2()
