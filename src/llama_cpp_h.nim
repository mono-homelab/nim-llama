# https://github.com/ggerganov/llama.cpp/blob/master/llama.h

const LibLlama = "./llama.cpp/libllama.so"

type
  llama_context* = object
  llama_model* = object
  llama_token* = int64
  llama_token_data* {.bycopy.} = object
    id*: llama_token # token id
    logit*: float32  # log-odds of the token
    p*: float32      # probability of the token
  llama_token_data_array* {.bycopy.} = object
    data*: ptr llama_token_data
    size*: csize_t
    sorted*: bool

  llama_progress_callback* = proc (progress: float32, ctx: pointer)

  llama_log_level* {.size: sizeof(cint).} = enum
    LLAMA_LOG_LEVEL_ERROR = 2,
    LLAMA_LOG_LEVEL_WARN = 3,
    LLAMA_LOG_LEVEL_INFO = 4

  llama_log_callback* = proc (level: llama_log_level, text: cstring,
      user_data: pointer)

  llama_context_params* {.bycopy.} = object
    seed*: uint32
    n_ctx*: int32
    n_batch*: int32
    n_gqa*: int32
    rms_norm_eps*: float32
    n_gpu_layers*: int32
    main_gpu*: int32
    tensor_split*: ptr float32
    rope_freq_base*: float32
    rope_freq_scale*: float32
    progress_callback*: llama_progress_callback
    progress_callback_user_data*: pointer
    low_vram*: bool
    mul_mat_q*: bool
    f16_kv*: bool
    logits_all*: bool
    vocab_only*: bool
    use_mmap*: bool
    use_mlock*: bool
    embedding*: bool

  llama_model_quantize_params* = object # Define this structure if needed

  llama_ftype* {.size: sizeof(cint).} = enum
    LLAMA_FTYPE_ALL_F32 = 0,
    LLAMA_FTYPE_MOSTLY_F16 = 1,     # except 1d tensors
    LLAMA_FTYPE_MOSTLY_Q4_0 = 2,    # except 1d tensors
    LLAMA_FTYPE_MOSTLY_Q4_1 = 3,    # except 1d tensors
    LLAMA_FTYPE_MOSTLY_Q4_1_SOME_F16 = 4, # tok_embeddings.weight and output.weight are F16
    LLAMA_FTYPE_MOSTLY_Q8_0 = 7,    # except 1d tensors
    LLAMA_FTYPE_MOSTLY_Q5_0 = 8,    # except 1d tensors
    LLAMA_FTYPE_MOSTLY_Q5_1 = 9,    # except 1d tensors
    LLAMA_FTYPE_MOSTLY_Q2_K = 10,   # except 1d tensors
    LLAMA_FTYPE_MOSTLY_Q3_K_S = 11, # except 1d tensors
    LLAMA_FTYPE_MOSTLY_Q3_K_M = 12, # exceptLLAMA_API float * llama_get_logits(struct llama_context * ctx); 1d tensors
    LLAMA_FTYPE_MOSTLY_Q5_K_S = 16, # except 1d tensors
    LLAMA_FTYPE_MOSTLY_Q5_K_M = 17, # except 1d tensors
    LLAMA_FTYPE_MOSTLY_Q6_K = 18,   # except 1d tensors


proc llama_log_set*(log_callback: llama_log_callback,
    user_data: pointer) {.cdecl, importc, dynlib: LibLlama.}
proc llama_max_devices*(): cint {.cdecl, importc, dynlib: LibLlama.}
proc llama_context_default_params*(): llama_context_params {.cdecl, importc,
    dynlib: LibLlama.}
proc llama_model_quantize_default_params*(): llama_model_quantize_params {.cdecl,
    importc, dynlib: LibLlama.}
proc llama_mmap_supported*(): bool {.cdecl, importc, dynlib: LibLlama.}
proc llama_mlock_supported*(): bool {.cdecl, importc, dynlib: LibLlama.}

proc llama_backend_init*(numa: bool) {.importc: "llama_backend_init",
    dynlib: LibLlama.}

proc llama_backend_free*() {.importc: "llama_backend_free",
    dynlib: LibLlama.}

proc llama_time_us*(): int64 {.importc, dynlib: LibLlama.}

proc llama_load_model_from_file*(
        path_model: cstring,
        params: llama_context_params): ptr llama_model {.importc,
            dynlib: LibLlama.}

proc llama_free_model*(model: ptr llama_model) {.importc, dynlib: LibLlama.}

proc llama_new_context_with_model*(
        model: ptr llama_model,
        params: llama_context_params): ptr llama_context {.importc,
            dynlib: LibLlama.}


proc llama_eval*(ctx: ptr llama_context, tokens: ptr llama_token,
    n_tokens: cint, n_past: cint,
    n_threads: cint): cint {.importc: "llama_eval",
    dynlib: LibLlama.}

proc llama_eval_embd*(ctx: ptr llama_context, embd: ptr float32, n_tokens: cint,
    n_past: cint, n_threads: cint): cint {.importc: "llama_eval_embd",
    dynlib: LibLlama.}

# TODO lora stuffs

proc llama_get_kv_cache_token_count*(ctx: ptr llama_context): cint {.importc,
    dynlib: LibLlama.}

proc llama_eval_export*(ctx: ptr llama_context, fname: cstring): cint {.importc,
    dynlib: LibLlama.}

proc llama_tokenize*(
        ctx: ptr llama_context,
        text: cstring,
        tokens: ptr llama_token,
        n_max_tokens: cint,
        add_bos: bool): cint {.importc, dynlib: LibLlama.}

proc llama_tokenize_with_model*(
        model: ptr llama_model,
        text: cstring,
        tokens: ptr llama_token,
        n_max_tokens: cint,
        add_bos: bool): cint {.importc, dynlib: LibLlama.}

proc llama_n_vocab*(ctx: ptr llama_context): cint {.importc, dynlib: LibLlama.}
proc llama_n_ctx*(ctx: ptr llama_context): cint {.importc, dynlib: LibLlama.}
proc llama_n_embd*(ctx: ptr llama_context): cint {.importc, dynlib: LibLlama.}

proc llama_token_bos*(ctx: ptr llama_context): llama_token {.importc,
    dynlib: LibLlama.}
proc llama_token_eos*(ctx: ptr llama_context): llama_token {.importc,
    dynlib: LibLlama.}
proc llama_token_nl*(ctx: ptr llama_context): llama_token {.importc,
    dynlib: LibLlama.}

proc llama_n_vocab_from_model*(model: ptr llama_model): cint {.importc,
    dynlib: LibLlama.}
proc llama_n_ctx_from_model*(model: ptr llama_model): cint {.importc,
    dynlib: LibLlama.}
proc llama_n_embd_from_model*(model: ptr llama_model): cint {.importc,
    dynlib: LibLlama.}

proc llama_model_type*(model: ptr llama_model, buf: cstring,
    buf_size: csize): cint {.importc, dynlib: LibLlama.}


proc llama_token_to_str*(ctx: ptr llama_context,
    token: llama_token): cstring {.cdecl, dynlib: LibLlama,
    importc: "llama_token_to_str".}


proc llama_get_logits*(ctx: ptr llama_context): ptr float32 {.cdecl,
    dynlib: LibLlama, importc: "llama_get_logits".}

proc llama_sample_token_greedy*(ctx: ptr llama_context,
    candidates: ptr llama_token_data_array): llama_token {.cdecl,
    dynlib: LibLlama, importc: "llama_sample_token_greedy".}


# import std/[sequtils]

# func getLogits*(ctx: ptr llama_context, n_tokens: int): seq[seq[cfloat]] =
#   let n_vocab = llama_n_vocab(ctx)
#   let logitsPtr = llama_get_logits(ctx)
#   result = newSeq[seq[cfloat]](n_tokens)
#   for i in 0..<n_tokens:
#     result[i] = newSeq[cfloat](n_vocab)
#   for i in 0..<n_tokens:
#     for j in 0..<n_vocab:
#       let offset = i * n_vocab + j
#       # let valuePtr = logitsPtr + offset
#       let valuePtr = cast[ptr float32](cast[int](logitsPtr) + offset * sizeof(float32))
#       result[i][j] = valuePtr[]


